//
//  File.swift
//  ios-sales
//
//  Created by Goh Bock Yang on 28/1/19.
//  Copyright © 2019 Goh Bock Yang. All rights reserved.
//

import Foundation
import Siesta
class SiestaService: Service {
   
    
    init(){
        super.init(baseURL: "http://localhost:8080")
        SiestaLog.Category.enabled = [.network, .pipeline, .observers]
    }
    let jsonDecoder = JSONDecoder()
    var create: Resource {return resource("/record/create")}
    var getAll: Resource {return resource("/record/type/all")}
    var getUserPurchases: Resource {return resource("/record")}
}

let api = SiestaService()

