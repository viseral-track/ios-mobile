//
//  RequestDTO.swift
//  ios-sales
//
//  Created by Goh Bock Yang on 28/1/19.
//  Copyright © 2019 Goh Bock Yang. All rights reserved.
//

struct RequestDTO: Codable {
    let actionType: String
    let recordType: String
    let itemName: String
    let sellerName: String
}
