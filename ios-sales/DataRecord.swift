//
//  DataRecord.swift
//  ios-sales
//
//  Created by Goh Bock Yang on 28/1/19.
//  Copyright © 2019 Goh Bock Yang. All rights reserved.
//

import Foundation

class DataRecord {
    let id: String
    let actionType: String
    let recordType: String
    let itemStatus: String
    let itemName: String
    let buyerName: String
    let sellerName: String
    let timestamp: String
    
    init(id:String, action:String, record:String, status:String, itemName:String, buyerName:String, sellerName:String, timestamp:String){
        self.id = id
        self.actionType = action
        self.recordType = record
        self.itemStatus = status
        self.itemName = itemName
        self.buyerName = buyerName
        self.sellerName = sellerName
        self.timestamp = timestamp
    }
    
    init(json: [String: Any]){
        debugPrint(json)
        self.id = json["id"] as! String
        self.actionType = json["actionType"] as! String
        self.recordType = json["recordType"] as! String
        self.itemStatus = json["itemStatus"] as! String
        self.itemName = json["itemName"] as! String
        self.buyerName = json["buyerName"] as! String
        self.sellerName = json["sellerName"] as! String
        self.timestamp = json["timestamp"] as! String
    }
}
