//
//  File.swift
//  ios-sales
//
//  Created by Goh Bock Yang on 27/1/19.
//  Copyright © 2019 Goh Bock Yang. All rights reserved.
//

import Foundation

protocol UsernameDelegate{
    func onUsernameLoaded(username: String)
}
