//
//  SalesViewController.swift
//  ios-sales
//
//  Created by Goh Bock Yang on 27/1/19.
//  Copyright © 2019 Goh Bock Yang. All rights reserved.
//

import UIKit
import iOSDropDown

class SalesViewController: UIViewController{
    
    @IBOutlet weak var actionTypeDropDown: DropDown!
    let service: SiestaService = SiestaService()
    var displayData: DataRecord?
    override func viewDidLoad() {
        super.viewDidLoad()
        actionTypeDropDown.optionArray = ["Item 1", "Item 2", "Item 3"]
        apiCall()
        // Do any additional setup after loading the view.
    }
    
    func apiCall(){
        api.getAll.request(.get)
            .onSuccess {data in debugPrint(data.jsonArray)}
            .onFailure {error in debugPrint(error)}
    }
    
    func onUsernameLoaded(username: String) {
        print("Username loaded: /(username)")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
